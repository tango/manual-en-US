# Standard template engine

Middleware renders is a go template render middlewaer for [Tango](https://gitea.com/lunny/tango).

## Installation

    go get gitea.com/tango/renders

## Simple Example

```Go
type RenderAction struct {
    renders.Renderer
}

func (x *RenderAction) Get() {
    x.Render("test.html", renders.T{
        "test": "test",
    })
}

func main() {
    t := tango.Classic()
    t.Use(renders.New(renders.Options{
        Reload: true,
        Directory: "./templates",
    }))
}
```

## Load files from Filesystem interface

```go
t.Use(renders.New(renders.Options{
        Reload: true,
        Directory: "./templates",
        Filesystem: http.Dir("./templates"),
    }))
```