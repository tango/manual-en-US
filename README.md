# Tango

Tango is a micro & pluggable web framework for Go.

##### Current version: v0.6.2   [Version History](https://gitea.com/lunny/tango/releases)

## Getting Started

To install Tango:

    go get gitea.com/lunny/tango

A classic usage of Tango below:

```go
package main

import (
    "errors"

    "gitea.com/lunny/tango"
)

type Action struct {
    tango.JSON
}

func (Action) Get() interface{} {
    if true {
        return map[string]string{
            "say": "Hello tango!",
        }
    }
    return errors.New("something error")
}

func main() {
    t := tango.Classic()
    t.Get("/", new(Action))
    t.Run()
}
```

Then visit `http://localhost:8000` on your browser. You will get
```
{"say":"Hello tango!"}
```

If you change `true` after `if` to `false`, then you will get
```
{"err":"something error"}
```

This code will automatically convert returned map or error to a json because we has an embedded struct `tango.JSON`.

## Features

- Powerful routing & Flexible routes combinations.
- Directly integrate with existing services.
- Easy to plugin features with modular design.
- High performance dependency injection embedded.

## Middlewares

Middlewares allow you easily plugin features for your Tango applications.

There are already many [middlewares](https://gitea.com/tango) to simplify your work:

- [recovery](https://gitea.com/lunny/tango/wiki/Recovery) - recover after panic
- [compress](https://gitea.com/lunny/tango/wiki/Compress) - Gzip & Deflate compression
- [static](https://gitea.com/lunny/tango/wiki/Static) - Serves static files
- [logger](https://gitea.com/lunny/tango/wiki/Logger) - Log the request & inject Logger to action struct
- [param](https://gitea.com/lunny/tango/wiki/Params) - get the router parameters
- [return](https://gitea.com/lunny/tango/wiki/Return) - Handle the returned value smartlly
- [context](https://gitea.com/lunny/tango/wiki/Context) - Inject context to action struct
- [session](https://gitea.com/tango/session) - [![Build Status](https://drone.io/gitea.com/tango/session/status.png)](https://drone.io/gitea.com/tango/session/latest) [![](http://gocover.io/_badge/gitea.com/tango/session)](http://gocover.io/gitea.com/tango/session) Session manager, [session-redis](http://gitea.com/tango/session-redis), [session-nodb](http://gitea.com/tango/session-nodb), [session-ledis](http://gitea.com/tango/session-ledis)
- [xsrf](https://gitea.com/tango/xsrf) - [![Build Status](https://drone.io/gitea.com/tango/xsrf/status.png)](https://drone.io/gitea.com/tango/xsrf/latest) [![](http://gocover.io/_badge/gitea.com/tango/xsrf)](http://gocover.io/gitea.com/tango/xsrf) Generates and validates csrf tokens
- [binding](https://gitea.com/tango/binding) - [![Build Status](https://drone.io/gitea.com/tango/binding/status.png)](https://drone.io/gitea.com/tango/binding/latest) [![](http://gocover.io/_badge/gitea.com/tango/binding)](http://gocover.io/gitea.com/tango/binding) Bind and validates forms
- [renders](https://gitea.com/tango/renders) - [![Build Status](https://drone.io/gitea.com/tango/renders/status.png)](https://drone.io/gitea.com/tango/renders/latest) [![](http://gocover.io/_badge/gitea.com/tango/renders)](http://gocover.io/gitea.com/tango/renders) Go template engine
- [dispatch](https://gitea.com/tango/dispatch) - [![Build Status](https://drone.io/gitea.com/tango/dispatch/status.png)](https://drone.io/gitea.com/tango/dispatch/latest) [![](http://gocover.io/_badge/gitea.com/tango/dispatch)](http://gocover.io/gitea.com/tango/dispatch) Multiple Application support on one server
- [tpongo2](https://gitea.com/tango/tpongo2) - [![Build Status](https://drone.io/gitea.com/tango/tpongo2/status.png)](https://drone.io/gitea.com/tango/tpongo2/latest) [![](http://gocover.io/_badge/gitea.com/tango/tpongo2)](http://gocover.io/gitea.com/tango/tpongo2) [Pongo2](https://github.com/flosch/pongo2) teamplte engine support
- [captcha](https://gitea.com/tango/captcha) - [![Build Status](https://drone.io/gitea.com/tango/captcha/status.png)](https://drone.io/gitea.com/tango/captcha/latest) [![](http://gocover.io/_badge/gitea.com/tango/captcha)](http://gocover.io/gitea.com/tango/captcha) Captcha
- [events](https://gitea.com/tango/events) - [![Build Status](https://drone.io/gitea.com/tango/events/status.png)](https://drone.io/gitea.com/tango/events/latest) [![](http://gocover.io/_badge/gitea.com/tango/events)](http://gocover.io/gitea.com/tango/events) Before and After
- [flash](https://gitea.com/tango/flash) - [![Build Status](https://drone.io/gitea.com/tango/flash/status.png)](https://drone.io/gitea.com/tango/flash/latest) [![](http://gocover.io/_badge/gitea.com/tango/flash)](http://gocover.io/gitea.com/tango/flash) Share data between requests
- [debug](https://gitea.com/tango/debug) - [![Build Status](https://drone.io/gitea.com/tango/debug/status.png)](https://drone.io/gitea.com/tango/debug/latest) [![](http://gocover.io/_badge/gitea.com/tango/debug)](http://gocover.io/gitea.com/tango/debug) show detail debug infomaton on log
- [basicauth](https://gitea.com/tango/basicauth) - [![Build Status](https://drone.io/gitea.com/tango/basicauth/status.png)](https://drone.io/gitea.com/tango/basicauth/latest) [![](http://gocover.io/_badge/gitea.com/tango/basicauth)](http://gocover.io/gitea.com/tango/basicauth) basicauth middleware
- [cache](https://gitea.com/tango/cache) - [![Build Status](https://drone.io/gitea.com/tango/cache/status.png)](https://drone.io/gitea.com/tango/cache/latest) [![](http://gocover.io/_badge/gitea.com/tango/cache)](http://gocover.io/gitea.com/tango/cache) cache middleware - cache-memory, cache-file, [cache-ledis](https://gitea.com/tango/cache-ledis), [cache-nodb](https://gitea.com/tango/cache-nodb), [cache-mysql](https://gitea.com/tango/cache-mysql), [cache-postgres](https://gitea.com/tango/cache-postgres), [cache-memcache](https://gitea.com/tango/cache-memcache), [cache-redis](https://gitea.com/tango/cache-redis)

## Getting Help

- [Wiki](https://gitea.com/lunny/tango/wiki)
- [API Reference](https://gowalker.org/gitea.com/lunny/tango)
- [Discuss](https://groups.google.com/forum/#!forum/go-tango)

## Cases

- [Wego](https://github.com/go-tango/wego) - Discuss Forum
- [dbweb](https://github.com/go-xorm/dbweb) - DB management web UI
- [Godaily](http://godaily.org) - [github](https://github.com/godaily/news)
- [Pugo](https://github.com/go-xiaohei/pugo) - A pugo blog
- [Gos](https://github.com/go-tango/gos) - Static web server
- [GoFtpd](https://github.com/goftp/ftpd) - Pure Go cross-platform ftp server

## License

This project is under BSD License. See the [LICENSE](LICENSE) file for the full license text.
