# Flash

Middleware flash is a tool for share data between requests for [Tango](https://gitea.com/lunny/tango).

## Installation

    go get gitea.com/tango/flash

## Simple Example

```Go

import "gitea.com/tango/session"

type FlashAction struct {
    flash.Flash
}

func (x *FlashAction) Get() {
    x.Flash.Set("test", "test")
}

func (x *FlashAction) Post() {
   x.Flash.Get("test").(string) == "test"
}

func main() {
    t := tango.Classic()
    sessions := session.Sessions()
    t.Use(flash.Flashes(sessions))
    t.Any("/", new(FlashAction))
    t.Run()
}
```
