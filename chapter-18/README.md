# Cache middleware

Cache middleware let your application save all kind of data. Storage includes memory, file, Redis, Memcache, PostgreSQL, MySQL, Ledis and Nodb.

## Installation

```Go
go get gitea.com/tango/cache
```
