# Binding

Middlware binding provides request data binding and validation for [Tango](https://gitea.com/lunny/tango).

## Installation

	go get gitea.com/tango/binding

## Example

```Go
import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/binding"
)

type Action struct {
    binding.Binder
}

type MyStruct struct {
    Id int64
    Name string
}

func (a *Action) Get() string {
    var mystruct MyStruct
    errs := a.Bind(&mystruct)
    return fmt.Sprintf("%v, %v", mystruct, errs)
}

func main() {
    t := tango.Classic()
    t.Use(binding.Bind())
    t.Get("/", new(Action))
    t.Run()
}
```

Visit `/?id=1&name=2` on your browser and you will find output
```
{1 sss}, []
```
