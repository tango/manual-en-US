# Session NoDB Store

## Installation

    go get gitea.com/tango/session-nodb

## Example

```Go
package main

import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/session"
    "gitea.com/tango/session-nodb"
)

type SessionAction struct {
    session.Session
}

func (a *SessionAction) Get() string {
    a.Session.Set("test", "1")
    return a.Session.Get("test").(string)
}

func main() {
    o := tango.Classic()
    store, _ := nodbstore.New(nodbstore.Options{
        Path:    "./nodbstore",
        DbIndex: 0,
        MaxAge:  30 * time.Minute,
    })
    o.Use(session.New(session.Options{
        Store: store,
        }))
    o.Get("/", new(SessionAction))
}
```
